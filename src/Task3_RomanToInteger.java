import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

/*
Римские цифры представлены семь различных символов: I, V, X, L, C, D и M.
Символ   значения
I           1
V           5
X           10
L           50
C           100
D           500
M           1000
Например, 2 записывается как II римской цифрой, просто складываются две единицы. 12 записывается как XII, что просто X + II.
Число 27 записывается как XXVII, что XX + V + II равно.

Римские цифры обычно записываются слева направо от наибольшего к наименьшему.
 Однако цифра для четырех таковой не является IIII. Вместо этого число четыре записывается как IV.
 Поскольку единица стоит перед пятеркой, мы вычитаем ее, получая четыре.
  Тот же принцип применим к числу девять, которое записывается как IX.
  Существует шесть случаев, когда используется вычитание:

I может быть помещен перед V (5) и X (10), чтобы получились 4 и 9.
X может быть помещен перед L (50) и C (100), чтобы получились 40 и 90.
C может быть помещен перед D (500) и M (1000), чтобы получились 400 и 900.
Если задана римская цифра, преобразуйте ее в целое число.
Example 1:
Input: s = "III"
Output: 3
Explanation: III = 3.

Example 2:
Input: s = "LVIII"
Output: 58
Explanation: L = 50, V= 5, III = 3.
 */
public class Task3_RomanToInteger {
    public static void main(String[] args) {
        Task3_RomanToInteger romanToInteger = new Task3_RomanToInteger();

        System.out.println(romanToInteger.romanToArabic("XIV"));

    }

    public int romanToArabic(String s) {
        HashMap<Character, Integer> map = new HashMap<>();
        map.put('I', 1);
        map.put('V', 5);
        map.put('X', 10);
        map.put('L', 50);
        map.put('C', 100);
        map.put('D', 500);
        map.put('M', 1000);

        int end = s.length() - 1;
        char[] arr = s.toCharArray();
        int arabic;
        int result = map.get(arr[end]);
        for (int i = end - 1; i >= 0 ; i--) {
            arabic = map.get(arr[i]);

            if (arabic < map.get(arr[i + 1])) {
                result -= arabic;
            } else {
                result += arabic;
            }
        }
        return result;
    }

}

/*
    enum RomanNumeral {
        I(1), IV(4), V(5), IX(9), X(10),
        XL(40), L(50), XC(90), C(100),
        CD(400), D(500), CM(900), M(1000);

        private int value;
        RomanNumeral(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }

        public static List<RomanNumeral> getReverseSortedValues() {
            return Arrays.stream(values())
                    .sorted(Comparator.comparing((RomanNumeral e) -> e.value).reversed())
                    .collect(Collectors.toList());
        }
    }

    public static int romanToArabic(String input) {
        String romanNumeral = input.toUpperCase();
        int result = 0;

        List<RomanNumeral> romanNumerals = RomanNumeral.getReverseSortedValues();

        int i = 0;

        while ((romanNumeral.length() > 0) && (i < romanNumerals.size())) {
            RomanNumeral symbol = romanNumerals.get(i);
            if (romanNumeral.startsWith(symbol.name())) {
                result += symbol.getValue();
                romanNumeral = romanNumeral.substring(symbol.name().length());
            } else {
                i++;
            }
        }

        if (romanNumeral.length() > 0) {
            throw new IllegalArgumentException(input + " cannot be converted to a Roman Numeral");
        }

        return result;
    }

 */

