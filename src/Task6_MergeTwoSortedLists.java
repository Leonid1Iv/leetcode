
/*
Вам будут предоставлены заголовки двух отсортированных связанных списков list1 и list2.
Объедините два списка в один отсортированный список. Список должен быть составлен путем объединения узлов первых двух списков.
Возвращает заголовок объединенного связанного списка.
Пример 1:

Ввод: list1 = [1,2,4], list2 = [1,3,4]
Вывод: [1,1,2,3,4,4]
Пример 2:

Ввод: list1 = [], list2 = []
Вывод: []
Пример 3:

Входные данные: list1 = [], list2 = [0]
Вывод: [0]

 */


public class Task6_MergeTwoSortedLists {
    public static void main(String[] args) {

       ListNode list1 = new ListNode(1);
       list1.next = new ListNode(2);
       list1.next.next = new ListNode(4);

       ListNode list2 = new ListNode(1);
       list2.next = new ListNode(3);
       list2.next.next = new ListNode(4);

       ListNode mergedList = mergeTwoLists(list1, list2);

       while (mergedList != null) {
           System.out.print(mergedList.val + " ");
           mergedList = mergedList.next;
       }
    }

    static class ListNode {
        int val;
        ListNode next;

        public ListNode(int val) {
            this.val = val;
        }
    }

    public static ListNode mergeTwoLists(ListNode list1, ListNode list2) {
        ListNode dummy = new ListNode(0);
        ListNode current = dummy;

        while (list1 != null && list2 != null) {
            if (list1.val <= list2.val) {
                current.next = list1;
                list1 = list1.next;
            } else {
                current.next = list2;
                list2 = list2.next;
            }
            current = current.next;
        }

        current.next = (list1 != null) ? list1 : list2;

        return dummy.next;
    }


}
