import java.math.BigInteger;
import java.util.Scanner;

public class Task13_AddBinary {
    public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
    String a = scanner.nextLine();
    String b = scanner.nextLine();
    Solution solution = new Solution();
    String result = solution.addBinary(a, b);
        System.out.println(result);
    }

    public static class Solution {
        public String addBinary(String a, String b) {
          /* BigInteger aOne = new BigInteger(a, 2);
            BigInteger bOne = new BigInteger(b, 2);
            BigInteger result = aOne.add(bOne);
            return result.toString(2);*/

            StringBuilder result = new StringBuilder();
            int i = a.length() - 1;
            int j = b.length() - 1;
            int carry = 0;
            while (i >= 0 || j >= 0) {
                int sum = carry;
                if (j >= 0)
                    sum += b.charAt(j--) - '0';
                if (i >= 0)
                    sum += a.charAt(i--) - '0';
                result.append(sum % 2);
                carry = sum / 2;
            }
            if (carry != 0)
                result.append(carry);
            return result.reverse().toString();
        }
    }
}
