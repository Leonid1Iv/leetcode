import java.util.Arrays;

/*
Напишите функцию для поиска самой длинной строки общего префикса среди массива строк.
Если общего префикса нет, верните пустую строку "".

Example 1:
Input: strs = ["flower","flow","flight"]
Output: "fl"

Example 2:
Input: strs = ["dog","racecar","car"]
Output: ""
Объяснение: Среди входных строк нет общего префикса.

 */
public class Task4_LongestCommonPrefix {
    public static void main(String[] args) {
        String[] strs = new String []{"flight", "flow", "flower"};
        System.out.println(longestCommonPrefix(strs));
    }



    public static String longestCommonPrefix(String[] strs) {
        Arrays.sort(strs);
        String s1 = strs[0];
        String s2 = strs[strs.length-1];
        int idx = 0;
        while(idx < s1.length() && idx < s2.length()){
            if(s1.charAt(idx) == s2.charAt(idx)){
                idx++;
            } else {
                break;
            }
        }
        return s1.substring(0, idx);
    }
}

