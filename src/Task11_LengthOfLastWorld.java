import java.util.Scanner;

public class Task11_LengthOfLastWorld {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите строку");
        String s = scanner.nextLine();
        Solution solution = new Solution();
        int m = solution.lengthOfLastWord(s);
        System.out.println(m);
    }

    public static class Solution {
        public int lengthOfLastWord(String s) {
            String str = s.trim();
            System.out.println(str);
            int lastIndex = str.length() - 1;
            int spaceIndex = str.lastIndexOf(" ");
            return lastIndex - spaceIndex;
        }
    }

}