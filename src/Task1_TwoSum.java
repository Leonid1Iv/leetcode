import java.util.Arrays;

/*
Учитывая массив целых чисел nums и целое число target, верните индексы двух чисел так, чтобы они в сумме равнялись target.
Вы можете предположить, что каждый ввод будет иметь ровно одно решение, и вы не можете использовать один и тот же элемент дважды.
Example 1:
Input: nums = [2,7,11,15], target = 9
Output: [0,1]
Explanation: Because nums[0] + nums[1] == 9, we return [0, 1].
 */
public class Task1_TwoSum {
    public static void main(String[] args) {
        int[] nums = {2,5,5,11};
        int target = 10;
        twoSum(nums, target);
    }
    public static void twoSum(int[] nums, int target) {
        int[] nums1 = new int[2];
        myBreakLabel:
        for (int i = 0; i < nums.length; i++) {
            for (int j = 1; j < nums.length; j++) {
                if (nums[i] + nums[j] == target && i != j ) {
                    nums1[0] = i;
                    nums1[1] = j;
                    System.out.println( Arrays.toString(nums1));
                    break myBreakLabel;
                }
            }
        }
    }
}
