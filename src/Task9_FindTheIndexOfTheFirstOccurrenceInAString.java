/*
Учитывая две строки needle и haystack, верните индекс первого вхождения needle в haystack,
 или -1 если needle не является частью haystack.

Example 1:

Input: haystack = "sadbutsad", needle = "sad"
Output: 0
Explanation: "sad" occurs at index 0 and 6.
The first occurrence is at index 0, so we return 0.
Example 2:

Input: haystack = "leetcode", needle = "leeto"
Output: -1

Объяснение: "leeto" не встречается в "leetcode", поэтому мы возвращаем -1.
 */

public class Task9_FindTheIndexOfTheFirstOccurrenceInAString {

    public static void main(String[] args) {
         String haystack = "leetcode", needle = "leeto";
        Solution solution = new Solution();
        int result = solution.strStr(haystack, needle);
        System.out.println(result);

    }

    public static class Solution  {
        public int strStr(String haystack, String needle) {
            return haystack.indexOf(needle);
        }
    }
}
