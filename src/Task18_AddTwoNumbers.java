/*
Вам предоставлены два непустых связанных списка, представляющих два неотрицательных целых числа.
 Цифры хранятся в обратном порядке, и каждый из их узлов содержит одну цифру.
 Сложите два числа и верните сумму в виде связанного списка.
Вы можете предположить, что эти два числа не содержат никакого начального нуля,
за исключением самого числа 0.

Пример 1:
Ввод: l1 = [2,4,3], l2 = [5,6,4]
Вывод: [7,0,8]
Пояснение: 342 + 465 = 807.

Пример 2:
Ввод: l1 = [0], l2 = [0]
Вывод: [0]

Пример 3:
Ввод: l1 = [9,9,9,9,9,9], l2 = [9,9,9,9]
Вывод: [8,9,9,9,0,0,0,1]

Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode() {}
 *     ListNode(int val) { this.val = val; }
 *     ListNode(int val, ListNode next) { this.val = val; this.next = next; }
 * }
 */


public class Task18_AddTwoNumbers {
    public static class ListNode {
        int val;
        ListNode next;

        public ListNode(int val) {
            this.val = val;
        }
    }

    public static void main(String[] args) {
        // Создание первого связанного списка: 2 -> 4 -> 3
        ListNode l1 = new ListNode(2);
        l1.next = new ListNode(4);
        l1.next.next = new ListNode(3);

        // Создание второго связанного списка: 5 -> 6 -> 4
        ListNode l2 = new ListNode(5);
        l2.next = new ListNode(6);
        l2.next.next = new ListNode(4);

        // Вызов функции сложения двух чисел, представленных связанными списками
        ListNode result = Solution.addTwoNumbers(l1, l2);

        // Вывод результата
        while (result != null) {
            System.out.print(result.val + " ");
            result = result.next;
        }

    }


    public static class Solution {
        public static ListNode addTwoNumbers(ListNode l1, ListNode l2) {
            ListNode dummyHead = new ListNode(0);
            ListNode p = l1, q = l2, current = dummyHead;
            int carry = 0;

            while (p != null || q != null) {
                int x = (p != null) ? p.val : 0;
                int y = (q != null) ? q.val : 0;
                int sum = carry + x + y;
                carry = sum / 10;
                current.next = new ListNode(sum % 10);
                current = current.next;
                if (p != null) p = p.next;
                if (q != null) q = q.next;
            }

            if (carry > 0) {
                current.next = new ListNode(carry);
            }

            return dummyHead.next;
        }
    }
}



