/*
Учитывая head вид отсортированного связанного списка, удалите все дубликаты таким образом, чтобы каждый элемент
 появлялся только один раз. Верните также отсортированный связанный список.

Example 1:
Input: head = [1,1,2]
Output: [1,2]

Example 2:
Input: head = [1,1,2,3,3]
Output: [1,2,3]
 */

public class Task16_RemoveDuplicatesFromSortedList {
    public static void main(String[] args) {
        Solution  solution = new Solution();
        ListNode head = new ListNode(1);
        head.next = new ListNode(1);
        head.next.next = new ListNode(2);
        ListNode result = solution.deleteDuplicates(head);




    }

    public static class ListNode {
      int val;
      ListNode next;
      ListNode() {}
     ListNode(int val) { this.val = val; }
      ListNode(int val, ListNode next) { this.val = val; this.next = next; }
  }


    public static class Solution {
        public ListNode deleteDuplicates(ListNode head) {
            ListNode current = head;

            while (current != null && current.next != null) {
                if (current.val == current.next.val) {
                    current.next = current.next.next;
                } else {
                    current = current.next;
                }
            }

            return head;
        }
    }
}
