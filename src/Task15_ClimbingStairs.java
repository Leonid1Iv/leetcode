import java.util.Scanner;

/*Вы поднимаетесь по лестнице. Чтобы добраться до верха, требуются n шаги.
Каждый раз вы можете подниматься по 1 или 2 ступенькам. Сколькими различными способами вы можете
подняться на вершину?

Пример 1:
Ввод: n = 2
Вывод:2
Объяснение: Есть два способа подняться на вершину.
1. 1 ступенька + 1 ступенька
2. 2 ступеньки

Пример 2:
Ввод: n = 3
Вывод:3
Объяснение: Есть три способа подняться на вершину.
1. 1 ступенька + 1 ступенька + 1 ступенька
2. 1 ступенька + 2 ступеньки
3. 2 ступеньки + 1 ступенька

 */
public class Task15_ClimbingStairs {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите кол-во шагов");
        int n = scanner.nextInt();
        Solution solution = new Solution();
        int result = solution.climbStairs(n);
        System.out.println(result);
    }

    public static class Solution{
        public int climbStairs(int n) {
            int[] arr = new int[n + 1];
            arr[0] = 1;
            arr[1] = 1;
            for (int i = 2; i <= n; i++) {
                arr[i] = arr[i - 1] + arr[i - 2];
            }
            return arr[n];
        }
    }
}
