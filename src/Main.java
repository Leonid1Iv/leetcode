public class Main {
    public static void main(String[] args) {
        // Создание узлей связанного списка
        ListNode head = new ListNode(1);
        ListNode second = new ListNode(2);
        ListNode third = new ListNode(3);

        // Связывание узлей в список
        head.next = second;
        second.next = third;

        // Вывод элементов связанного списка
        ListNode current = head;
        while (current != null) {
            System.out.println(current.val);
            current = current.next;
        }
    }
    static class ListNode {
        int val;
        ListNode next;

        public ListNode(int val) {
            this.val = val;
        }
    }






}