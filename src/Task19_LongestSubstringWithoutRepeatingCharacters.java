import java.util.Scanner;

/*
Для заданной строки s найдите длину самой длинной подстрока без повторяющихся символов.

Пример 1:
Ввод: s = "abcabcbb"
Вывод:3
Объяснение: Ответ "abc" длиной 3.

Пример 2:
Ввод: s = "bbbbb"
Вывод:1
Объяснение: Ответ "b" длиной 1.

Пример 3:
Ввод: s = "pwwkew"
Вывод:3
Пояснение: Ответ "wke" длиной 3.
Обратите внимание, что ответ должен быть подстрокой, "pwke" - это подпоследовательность, а не подстрока.

 */
public class Task19_LongestSubstringWithoutRepeatingCharacters {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String s = scanner.nextLine();
        System.out.println(lengthOfLongestSubstring(s));
    }

    public static int lengthOfLongestSubstring(String s) {
        int maxLength = 0;
        int[] charIndex = new int[128]; // Assuming ASCII characters

        for (int end = 0, start = 0; end < s.length(); end++) {
            start = Math.max(charIndex[s.charAt(end)], start);
            maxLength = Math.max(maxLength, end - start + 1);
            charIndex[s.charAt(end)] = end + 1;
        }

        return maxLength;
    }
}
