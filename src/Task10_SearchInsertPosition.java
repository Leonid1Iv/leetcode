/*
Учитывая отсортированный массив различных целых чисел и целевое значение,
верните индекс, если целевое значение найдено.
Если нет, верните индекс туда, где он был бы, если бы он был вставлен по порядку.

Пример 1:
Ввод: nums = [1,3,5,6], target = 5
Вывод: 2

Пример 2:
Ввод: nums = [1,3,5,6], target = 2
Вывод: 1

Пример 3:
Ввод: nums = [1,3,5,6], target = 7
Вывод: 4
 */
public class Task10_SearchInsertPosition {
    public static void main(String[] args) {
        int[] nums = {1, 3, 5, 6};
        int target = 5;
        Solution solution = new Solution();
        int result = solution.searchInsert(nums, target);
        System.out.println(result);
    }

    public static class Solution {
        public int searchInsert(int[] nums, int target) {
            int left = 0;
            int right = nums.length - 1;
            while (left <= right) {
                int mid = (left + right) / 2;
                if (nums[mid] == target) {
                    return mid;
                } else if (nums[mid] < target) {
                    left = mid + 1;
                } else {
                    right = mid - 1;
                }
            }
            return left;
        }
    }


}
