import java.util.Arrays;
import java.util.Scanner;

public class Task12_PlusOne {
   public static void main(String[] args) {
       Scanner scanner = new Scanner(System.in);
       int array = scanner.nextInt();
       String resultStrin = String.valueOf(array);
       int[] resultArr = new int[resultStrin.length()];
       for (int i = 0; i < resultStrin.length(); i++) {
           resultArr[i] = resultStrin.charAt(i) - '0';
       }
       Solution solution = new Solution();
       int[] result = solution.plusOne(resultArr);
       for (int j : result) {
           System.out.print(j + " ");

       }
   }
    public static class Solution {
        public int[] plusOne(int[] digits) {
            int number = 0;
            for (int digit : digits) {
                number = number * 10 + digit;
            }
            int result = number + 1;
            String resultString = String.valueOf(result);
            int[] resultArray = new int[resultString.length()];
            for (int i = 0; i < resultString.length(); i++) {
                resultArray[i] = resultString.charAt(i) - '0';
            }
            return resultArray;

        }
    }
}
