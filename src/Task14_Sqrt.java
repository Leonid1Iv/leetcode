import java.util.Scanner;

public class Task14_Sqrt {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int x = scanner.nextInt();

        Solution solution = new Solution();
        int result = solution.mySqrt(x);
        System.out.println(result);
    }

    public static class Solution {
        public int mySqrt (int x) {
            double result = Math.sqrt(x);
            return (int) Math.floor(result);
        }
    }
}
