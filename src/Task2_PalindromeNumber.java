import java.util.Arrays;

/*
Задано целое число x, верните, true если x это палиндром в противном false.
Example 1:
Input: x = 121
Output: true
Explanation: 121 reads as 121 from left to right and from right to left.
 */
public class Task2_PalindromeNumber {
    public static void main(String[] args){
        System.out.println(isPalindrome(10));
    }

    public static boolean isPalindrome(int x) {
        String numStr = String.valueOf(x);
        char[] result = numStr.toCharArray();
        char[] isresult = new char[result.length];
        new StringBuilder(result.length).append(result).reverse().getChars(0, result.length, isresult, 0);
        return Arrays.equals(result, isresult);





    }





}
